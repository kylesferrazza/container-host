{ pkgs, ... }:
let
  proxycontainers = builtins.fetchGit {
    url = "https://github.com/kylesferrazza/proxycontainers.git";
    rev = "1f99161fae978ae19961d69e9f9964beed44c502";
  };
in {
  imports = [
    <nixpkgs/nixos/modules/virtualisation/google-compute-image.nix>
    proxycontainers.outPath
  ];

  users.users.root.openssh.authorizedKeys.keyFiles = [ ~/.ssh/id_rsa.pub ];

  security.acme = {
    email = "kyle.sferrazza@gmail.com";
    acceptTerms = true;
  };

  proxycontainers = {
    enable = true;
    containers = import ./containers.nix;
    rootDomain = "nix.kylesferrazza.com";
  };
}
